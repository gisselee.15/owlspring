package py.com.owl.owlspring.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "cliente_codigo_uk", columnNames = { "codigo" }) })
public class Cliente {

	private static final String SECUENCIA = "cliente_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{cliente.codigo.notNull}")
	@NotBlank(message = "{cliente.codigo.notBlank}")
	@Size(max = 10, message = "{.codigo.size}")
	private String codigo;

	@NotNull(message = "{cliente.nombre.notNull}")
	@NotBlank(message = "{cliente.nombre.notBlank}")
	@Size(max = 100, message = "{cliente.nombre.size}")
	private String nombre;

	@NotNull(message = "{cliente.apellido.notNull}")
	@NotBlank(message = "{cliente.apellido.notBlank}")
	@Size(max = 100, message = "{cliente.apellido.size}")
	private String apellido;

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getcodigo() {

		return codigo;
	}

	public void setcodigo(String codigo) {

		this.codigo = codigo;
	}

	public String getNombre() {

		return nombre;
	}

	public void setNombre(String nombre) {

		this.nombre = nombre;
	}

	public String getApellido() {

		return apellido;
	}

	public void setApellido(String apellido) {

		this.apellido = apellido;
	}

}
