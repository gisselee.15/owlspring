package py.com.owl.owlspring.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "persona_cedula_uk", columnNames = { "cedula" }) })
public class Persona {

	private static final String SECUENCIA = "persona_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SECUENCIA)
	@SequenceGenerator(name = SECUENCIA, sequenceName = SECUENCIA, allocationSize = 1)
	private Long id;

	@NotNull(message = "{persona.cedula.notNull}")
	@NotBlank(message = "{persona.cedula.notBlank}")
	@Size(max = 10, message = "{persona.cedula.size}")
	private String cedula;

	@NotNull(message = "{persona.nombre.notNull}")
	@NotBlank(message = "{persona.nombre.notBlank}")
	@Size(max = 100, message = "{persona.nombre.size}")
	private String nombre;

	@NotNull(message = "{persona.apellido.notNull}")
	@NotBlank(message = "{persona.apellido.notBlank}")
	@Size(max = 100, message = "{persona.apellido.size}")
	private String apellido;
	
	
	@Lob
	private byte[] foto;
	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	// private transient MultipartFile multipartFile;
	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getCedula() {

		return cedula;
	}

	public void setCedula(String cedula) {

		this.cedula = cedula;
	}

	public String getNombre() {

		return nombre;
	}

	public void setNombre(String nombre) {

		this.nombre = nombre;
	}

	public String getApellido() {

		return apellido;
	}

	public void setApellido(String apellido) {

		this.apellido = apellido;
	}

}
