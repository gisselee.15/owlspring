package py.com.owl.owlspring.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.owlspring.dao.ClienteDao;
import py.com.owl.owlspring.domain.Cliente;

@Repository
@SessionScope
public class ClienteDaoImpl implements ClienteDao {

	@Autowired
	private EntityManager em;

	@Override
	public Cliente find(Long id) {

		return em.find(Cliente.class, id);

	}

	@Override
	public void insert(Cliente cliente) {

		em.persist(cliente);
	}

	@Override
	public void delete(Cliente cliente) {

		em.remove(cliente);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> getList() {

		String jpql = "SELECT object(P) FROM Cliente AS P";
		Query query = em.createQuery(jpql);
		return query.getResultList();
	}

}
