package py.com.owl.owlspring.dao;

import java.util.List;

import py.com.owl.owlspring.domain.Persona;

public interface PersonaDao {

	void insert(Persona persona);

	Persona find(Long id);

	Persona find(String cedula);

	void delete(Persona persona);

	List<Persona> getList();
	
	void update(Persona persona);

}
