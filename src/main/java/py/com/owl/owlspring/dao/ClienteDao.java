package py.com.owl.owlspring.dao;

import java.util.List;

import py.com.owl.owlspring.domain.Cliente;

public interface ClienteDao {

	void insert(Cliente cliente);

	Cliente find(Long id);

	void delete(Cliente cliente);

	List<Cliente> getList();

}
