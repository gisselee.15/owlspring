package py.com.owl.owlspring.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.owlspring.dao.PersonaDao;
import py.com.owl.owlspring.domain.Persona;

@Repository
@SessionScope
public class PersonaDaoImpl implements PersonaDao {

	@Autowired
	private EntityManager em;

	@Override
	public Persona find(Long id) {

		return em.find(Persona.class, id);

	}

	@Override
	public void insert(Persona persona) {

		em.persist(persona);
	}

	@Override
	public void delete(Persona persona) {

		em.remove(persona);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> getList() {

		String jpql = "SELECT object(P) FROM Persona AS P";
		Query query = em.createQuery(jpql);
		return query.getResultList();
	}

	@Override
	public Persona find(String cedula) {
		String jpql = "SELECT object(P) FROM Persona AS P Where cedula = ?1";
		Query query = em.createQuery(jpql);
		query.setParameter(1, cedula);
		try {
			return (Persona) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public void update(Persona persona) {
		// UPDATE persona SET cedula ´12345´ where id = 1;
		em.merge(persona);
	}
	

}
