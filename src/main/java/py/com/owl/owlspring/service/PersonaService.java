package py.com.owl.owlspring.service;

import java.util.List;

import py.com.owl.owlspring.domain.Persona;

public interface PersonaService {

	void insert(Persona persona);

	Persona find(Long id);

	Persona find(String cedula);

	void delete(Persona persona);

	List<Persona> getList();
	void update(Persona persona);
}
