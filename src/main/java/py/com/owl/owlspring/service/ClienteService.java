package py.com.owl.owlspring.service;

import java.util.List;
import py.com.owl.owlspring.domain.Cliente;

public interface ClienteService {

	void insert(Cliente cliente);

	Cliente find(Long id);

	void delete(Cliente cliente);

	List<Cliente> getList();
}
