package py.com.owl.owlspring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.owlspring.dao.ClienteDao;
import py.com.owl.owlspring.domain.Cliente;
import py.com.owl.owlspring.service.ClienteService;

@Service
@SessionScope
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteDao ClienteDao;

	@Override
	@Transactional
	public void insert(Cliente cliente) {

		cliente.setId(null);
		ClienteDao.insert(cliente);

	}

	@Override
	@Transactional
	public Cliente find(Long id) {

		return ClienteDao.find(id);
	}

	@Override
	@Transactional
	public void delete(Cliente cliente) {

		ClienteDao.delete(cliente);

	}

	@Override
	@Transactional
	public List<Cliente> getList() {

		return ClienteDao.getList();
	}

}
