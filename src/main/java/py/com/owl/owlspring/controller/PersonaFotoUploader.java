package py.com.owl.owlspring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.multipart.MultipartFile;

import py.com.owl.owlspring.domain.Persona;
import py.com.owl.owlspring.service.PersonaService;
import py.com.owl.owlspring.util.Log;


@RestController
@RequestScope
public class PersonaFotoUploader {

	//private Log log = new Log(getClass());
	@Autowired // Spring maneja la instancia
	private PersonaService personaService;
	
	@PostMapping("/persona/actualiza/foto")
	private ResponseEntity<Persona> actualizarFoto(@RequestParam() MultipartFile file,@RequestParam Long id){
		
			 
				Persona per = personaService.find(id);
				if (per == null) {
					//log.info("No se encontró persona con id: {}", id);
					return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
				}
				//log.info("se recibe archivo,: {}", file.getOriginalFilename());
				try {
					per.setFoto(file.getBytes());
					personaService.update(per);
					return ResponseEntity.ok(per);
				}catch (Exception ex) {
				//	log.error("no se pudo obtener archivo.getBytes()");
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
		
	}
	
	@PostMapping("/persona/actualiza/foto2")
	private ResponseEntity<Persona> actualizarFoto2(@RequestParam Long id){
		
			   
				
					return new ResponseEntity<>(HttpStatus.OK);
				
	}
	
	
	
	
	
	
}
