package py.com.owl.owlspring.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.owlspring.domain.Persona;
import py.com.owl.owlspring.service.PersonaService;
import py.com.owl.owlspring.util.Log;

@RestController
@SessionScope
public class PersonaController {

	private Log log = new Log(getClass());
	@Autowired
	private PersonaService personaService;

	@GetMapping("/persona/{id}")
	public ResponseEntity<Persona> buscar(@PathVariable Long id) {

		try {
			Persona per = personaService.find(id);
			if (per == null) {
				log.info("No se encontró persona con id: {}", id);
				return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
			} else {
				return ResponseEntity.ok(per);
			}
		} catch (Exception ex) {

			log.error("No se pudo buscar datos de persona", ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/persona")
	public ResponseEntity<List<Persona>> getList() {

		try {
			List<Persona> list = personaService.getList();
			log.info("Se encontraron datos de {} personas", list.size());

			return ResponseEntity.ok(list);
		} catch (Exception ex) {
			log.error("No se pudo obtener lista de personas", ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/persona")
	public ResponseEntity<Persona> add(@Valid Persona persona, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Errores de validación: {}", bindingResult.getFieldErrors());
			return ResponseEntity.badRequest().body(persona);
		} else {
			// si no hay errores de validación, intentaremos guardar
			try {
				personaService.insert(persona);
				return new ResponseEntity<>(persona, HttpStatus.CREATED);
			} catch (Exception ex) {
				log.error("No se pudo guardar datos de persona", ex);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@GetMapping("/persona/cedula/{cedula}")
	public ResponseEntity<Persona> buscar(@PathVariable String cedula) {
		try {
			Persona p = personaService.find(cedula);

			if (p == null) {

				return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
			} else {
				return ResponseEntity.ok(p);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
