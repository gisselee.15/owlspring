package py.com.owl.owlspring.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.owlspring.domain.Cliente;
import py.com.owl.owlspring.service.ClienteService;
import py.com.owl.owlspring.util.Log;

@RestController
@SessionScope
public class ClienteController {

	private Log log = new Log(getClass());
	@Autowired
	private ClienteService clienteService;

	@GetMapping("/cliente/{id}")
	public ResponseEntity<Cliente> buscar(@PathVariable Long id) {

		try {
			Cliente per = clienteService.find(id);
			if (per == null) {
				log.info("No se encontró cliente con id: {}", id);
				return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
			} else {
				return ResponseEntity.ok(per);
			}
		} catch (Exception ex) {

			log.error("No se pudo buscar datos de cliente", ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/cliente")
	public ResponseEntity<List<Cliente>> getList() {

		try {
			List<Cliente> list = clienteService.getList();
			log.info("Se encontraron datos de {} clientes", list.size());

			return ResponseEntity.ok(list);
		} catch (Exception ex) {
			log.error("No se pudo obtener lista de clientes", ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/cliente")
	public ResponseEntity<Cliente> add(@Valid Cliente cliente, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			log.info("Errores de validación: {}", bindingResult.getFieldErrors());
			return ResponseEntity.badRequest().body(cliente);
		} else {
			// si no hay errores de validación, intentaremos guardar
			try {
				clienteService.insert(cliente);
				return new ResponseEntity<>(cliente, HttpStatus.CREATED);
			} catch (Exception ex) {
				log.error("No se pudo guardar datos de cliente", ex);
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
}
