package py.com.owl.owlspring.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log {

	private final Logger logger;

	public Log(Class<?> clazz) {

		logger = LoggerFactory.getLogger(clazz);
	}

	public void info(String msg, Object ... args) {

		logger.info(msg, args);
	}

	public void debug(String msg, Object ... args) {

		logger.debug(msg, args);
	}

	public void warning(String msg, Object ... args) {

		logger.warn(msg, args);
	}

	public void warning(String msg, Throwable thrown, Object ... args) {

		logger.warn(msg, args);
	}

	public void error(String msg, Throwable thrown, Object ... args) {

		logger.error("", thrown);
	}

	public void error(String msg, Object ... args) {

		logger.error(msg, args);
	}
}
